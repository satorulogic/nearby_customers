from setuptools import setup

setup(
    name='nearby_customers',
    version='0.1',
    description='Help you find nearby customers',
    url='https://bitbucket.org/satorulogic/nearby_customers',
    author='satoru',
    classifiers=[
        'Programming Language :: Python :: 3.5',
    ],
    entry_points={
        'console_scripts': [
            'nearby_customers=nearby_customers:main'
        ]
    }
)
