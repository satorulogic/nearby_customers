import json
import os.path
import argparse

import sphere

HERE = os.path.abspath(os.path.dirname(__file__))

EARTH_RADIUS_IN_KM = 6371


def iter_customers_from_file(f_name):
    with open(f_name) as f:
        for l in f:
            data = json.loads(l)
            data['latitude'] = float(data['latitude'])
            data['longitude'] = float(data['longitude'])
            yield data


def calculate_distances(customers, dest_coords):
    dest_lat, dest_lon = dest_coords
    for c in customers:
        yield c, sphere.calculate_distance(
            c['latitude'], c['longitude'],
            dest_lat, dest_lon,
            EARTH_RADIUS_IN_KM
        )


def filter_by_distance(customer_distances, max_distance):
    for c, distance in customer_distances:
        if distance <= max_distance:
            yield c, distance


def get_nearby_customers(data_file, here_coords, max_distance):
    customers = iter_customers_from_file(data_file)
    customer_distance_pairs = calculate_distances(customers, here_coords)
    customer_distance_pairs = filter_by_distance(
        customer_distance_pairs,
        max_distance
    )
    for c, _ in sorted(customer_distance_pairs, key=lambda p: p[0]['user_id']):
        yield c


def create_arg_parser():
    parser = argparse.ArgumentParser(description="Output nearby customers")
    parser.add_argument(
        'customer_file',
        nargs="?",
        default=os.path.join(HERE, "customers.json"),
        help="The path of a text file with one customer per line, JSON-encoded"
    )
    parser.add_argument(
        '--here_coords',
        type=parse_coordinates_string,
        default=(53.3381985, -6.2592576),
        help="GPS coordinates of your position, eg. 53.3381985, -6.2592576"
    )
    parser.add_argument('--max_distance', default=100, type=float)
    return parser


def parse_coordinates_string(s):
    err_msg = "%r is not a valid coordinate"
    try:
        lat, lon = s.split(',', 1)
    except ValueError:
        raise argparse.ArgumentTypeError(err_msg % s)

    lat = lat.strip()
    lon = lon.strip()
    try:
        return float(lat), float(lon)
    except ValueError:
        raise argparse.ArgumentTypeError(err_msg % s)


def main():
    parser = create_arg_parser()
    args = parser.parse_args()
    for c in get_nearby_customers(
        args.customer_file, args.here_coords, args.max_distance
    ):
        print("{name}\t{user_id}".format(**c))

if __name__ == '__main__':
    main()
