import os.path
import argparse
from unittest.mock import patch

import pytest

import nearby_customers as nc

HERE = os.path.abspath(os.path.dirname(__file__))


def test_read_customers_from_file():
    test_file = os.path.join(HERE, "fake-customers.json")
    customers = nc.iter_customers_from_file(test_file)
    customers = list(customers)
    assert len(customers) == 2
    assert customers == [
        {
            "latitude": 53.807778,
            "user_id": 28,
            "name": "Charlie Halligan",
            "longitude": -7.714444
        },
        {
            "latitude": 53.4692815,
            "user_id": 7,
            "name": "Frank Kehoe",
            "longitude": -9.436036
        }
    ]


def test_calculate_distances():
    with patch.object(nc.sphere, 'calculate_distance') as calc_dist:
        calc_dist.return_value = 42

        customer = {
            "latitude": 53.807778,
            "user_id": 28,
            "name": "Charlie Halligan",
            "longitude": -7.714444
        }
        dest = (53, -6)

        pairs = nc.calculate_distances([customer], dest)
        pairs = list(pairs)

        assert len(pairs) == 1
        assert pairs[0] == (customer, 42)


def test_filter_by_distance():
    customer_distance_pairs = [
        ({"name": "Uno"}, 1),
        ({"name": "Dos"}, 1984),
        ({"name": "Tres"}, 3),
        ({"name": "Cuatro"}, 10),
    ]
    test_cases = [
        (9, ['Uno', 'Tres']),
        (1000, ['Uno', 'Tres', 'Cuatro']),
    ]
    for max_distance, expected_names in test_cases:
        result_names = [
            c['name'] for c, _ in
            nc.filter_by_distance(customer_distance_pairs, max_distance)
        ]
        assert result_names == expected_names


def test_get_nearby_customers():
    fname = "recent-customers.json"
    our_coords = (53.3381985, -6.2592576)
    with patch.object(nc, 'iter_customers_from_file') as iter_customers:
        customers = iter_customers.return_value = [
            {
                "latitude": 53.2451022,
                "user_id": 26,
                "name": "Stephen McArdle",
                "longitude": -6.238335
            },
            {
                "latitude": 53.038056,
                "user_id": 4,
                "name": "Ian Kehoe",
                "longitude": -7.653889
            },
            {
                "latitude": 52.833502,
                "user_id": 25,
                "name": "David Behan",
                "longitude": -8.522366
            }
        ]

        result = list(nc.get_nearby_customers(fname, our_coords, 50))
        assert len(result) == 1
        assert result[0] == customers[0]

        result = list(nc.get_nearby_customers(fname, our_coords, 100))
        assert len(result) == 2
        assert result == [customers[1], customers[0]]


class TestCoordinatesString:

    def test_should_return_a_pair_of_degrees(self):
        coords = nc.parse_coordinates_string("53.3381985, -6.2592576")
        assert (53.3381985, -6.2592576) == coords

    def test_should_raise_error_if_invalid(self):
        invalid_inputs = [
            "42",
            "53.3381985,",
            ",33",
            "abc,efg",
        ]
        for s in invalid_inputs:
            with pytest.raises(argparse.ArgumentTypeError):
                nc.parse_coordinates_string(s)
