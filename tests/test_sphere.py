from sphere import calculate_distance


class TestCalculateDistance:

    def test_known_distance_found_from_web(self):
        distance = calculate_distance(
            52.2296756, 21.0122287, 52.406374, 16.9251681, 6373
        )
        assert round(distance, 3) == 278.546

    def test_symmetry(self):
        lat1, lon1 = 53.3381985, -6.2592576
        lat2, lon2 = 52.228056, -7.915833
        assert (
            calculate_distance(lat1, lon1, lat2, lon2, 6371) ==
            calculate_distance(lat2, lon2, lat1, lon1, 6371)
        )

    def test_relative_distance(self):
        lat1, lon1 = 53.3381985, -6.2592576
        lat2, lon2 = 52.228056, -7.915833
        assert (
            calculate_distance(lat1, lon1, lat2, lon2, 6371) <
            calculate_distance(lat1 + 10, lon1, lat2, lon2, 6371)
        )
