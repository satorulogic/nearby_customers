Nearby Customers
===

## Installation

1. Make sure you have Python 3 installed
1. `cd` into this directory
1. Run `python setup.py install`, or `python setup.py develop` if you want to contribute

## Usage

Once installed, you'll have access to the `nearby_customers` command.

```bash
$ nearby_customers
Ian Kehoe       4
Nora Dempsey    5
Theresa Enright 6
Eoin Ahearn     8
Richard Finnegan        11
Christina McArdle       12
Olive Ahearn    13
Michael Ahearn  15
Patricia Cahill 17
Eoin Gallagher  23
Rose Enright    24
Stephen McArdle 26
Oliver Ahearn   29
Nick Enright    30
Alan Behan      31
Lisa Ahearn     39
```

## Development

1. `python setup.py develop`
1. `pip install -r test-requirements.txt`
1. `py.test tests`
