from math import (
    sin,
    cos,
    acos,
    radians,
)


def calculate_distance(lat1, lon1, lat2, lon2, radius):
    """Return the distance between two points on a sphere of the specified radius.

    Latitude and longtitude should be specified in degrees.
    The distance is calculated according to the first formula from

        https://www.wikiwand.com/en/Great-circle_distance#/Formulas
    """
    phi1 = radians(lat1)
    lambda1 = radians(lon1)
    phi2 = radians(lat2)
    lambda2 = radians(lon2)

    delta_lambda = lambda2 - lambda1

    delta_sigma = acos(
        sin(phi1) * sin(phi2) +
        cos(phi1) * cos(phi2) * cos(delta_lambda)
    )
    return radius * delta_sigma
